
// Create an object for quicksearch data.
Drupal.quicksearch = { }

/**
 *  Auto attach quicksearch features.
 */
Drupal.quicksearch.autoAttach = function() {
  $('input.autocomplete').each(function () {
    Drupal.quicksearch.input = $('#' + this.id.substr(0, this.id.length - 13)).attr('autocomplete', 'OFF')[0];
    
    // Only add quicksearch functionality to quicksearch configured forms.
    if (Drupal.quicksearch.form(Drupal.quicksearch.input.id)) {
      // Add a label to display inside the textfield only if the textfield has no value.
      $(Drupal.quicksearch.input).val(Drupal.settings.quicksearch.fieldLabel).bind('click', function() { if($(this).val() == Drupal.settings.quicksearch.fieldLabel) { $(this).val(''); } });
      $(Drupal.quicksearch.input).val(Drupal.settings.quicksearch.fieldLabel).bind('blur', function() { if($(this).val() == '') { $(this).val(Drupal.settings.quicksearch.fieldLabel); }  });
      
      // Hide the search form's submit button.
      $('input[@type=submit]||[@type=image]', Drupal.quicksearch.input.form).hide();
      // Setup craqbox.
      if (!Drupal.settings.quicksearch.selector) {
        Drupal.quicksearch.craqbox = $.craqbox(Drupal.quicksearch.input, {
          type: 'dom', 
          title: Drupal.settings.quicksearch.title,
          attachEvent: null,
          width: Drupal.settings.quicksearch.uiWidth,
          closeButtonTitle: Drupal.settings.quicksearch.closeButton,
          verticalAlign: false
        });
      }
    }
  });
};

/**
 *  Setup the search pager queue to use AJAX vs its actual links.
 */
Drupal.quicksearch.pagerSetup = function(db) {
  var parent;
  
  // Setup pager caching.
  Drupal.quicksearch.pagerCache = Drupal.quicksearch.pagerCache || [];
  
  // Grab the right parent element based on what type of display quicksearch is setup for.
  if (Drupal.settings.quicksearch.selector) {
    parent = $(Drupal.settings.quicksearch.selector);
  } else {
    var craqbox = Drupal.quicksearch.craqbox;
    parent = $('#'+ craqbox.o.craqboxId +' .'+ craqbox.o.contentClass).get(0);
  }
  
  // Loop through the links in the pager turning the links into AJAX.
  $('.pager a', parent).each(function() {
    $(this).click(function(event) {
      // Cache id for this content.
      var key = this.href.replace('http://', '').replace(/\//g, '_');
      if (Drupal.quicksearch.pagerCache[key]) {
        Drupal.quicksearch.display(db, Drupal.quicksearch.pagerCache[key]);
      } else {
        $.get(this.href, function(results) {
          Drupal.quicksearch.pagerCache[key] = results;
          Drupal.quicksearch.display(db, results);
        });
      }
      
      return false;
    })
  });
};

/**
 *  Setup content / user tabs to load via AJAX.
 */
Drupal.quicksearch.tabSetup = function(db) {
  var parent;
  
  // Use the right parent.
  if (Drupal.settings.quicksearch.selector) {
    parent = $('#'+ Drupal.settings.quicksearch.selector);    
  } else {
    parent = $('#'+ Drupal.quicksearch.craqbox.o.craqboxId);
  }
  
  // Add AJAX functionality.
  $('.quicksearch-tabs a', parent).each(function() {
    if ($(this).is('.active')) {
      Drupal.quicksearch.searchType = $(this).parent().id().replace('quicksearch-', '').replace('-link', '');
    }
    $(this).
      click(function(event) {
        if (Drupal.quicksearch.searchTab != $(this).html()) {
          $.get(this.href, function(results) {
            Drupal.quicksearch.display(db, results);
          });
          // Save this tab name as the current tab to avoid unnecessary AJAX calls.
          Drupal.quicksearch.searchTab = $(this).html();
        }
        return false;
      });
  });
};

/**
 *  Handle displaying the search results.
 */
Drupal.quicksearch.display = function(db, results) {
  // Search results added to an element.
  if (Drupal.settings.quicksearch.selector) {
    var el = $(Drupal.settings.quicksearch.selector);
    // A close button so the original page content can be restored.
    var closeButton = $('<a>')
    .attr({ title: Drupal.settings.quicksearch.closeButtonTitle, className: 'quicksearch_close_button', href: '#close' })
    .click(function() { Drupal.quicksearch.remove(db.input); return false; })
    .html(Drupal.settings.quicksearch.closeButtonLabel);
    
    // Cache the original page content before quicksearch occurred.
    if ($('#quicksearch_cache').length == 0) {
      $('<div>').attr('id', 'quicksearch_cache').css('display', 'none').append($(el).children()).appendTo(document.body);
    }
    $(el).fadeOut('fast', function() { 
      $(el).html(results).prepend(closeButton).fadeIn('fast');
    });
    Drupal.quicksearch.pagerSetup(db);
    Drupal.quicksearch.tabSetup(db);
  }
  // Search results displayed in craqbox.
  else {
    // Save the original parent so we can restore the element to its original state.
    if (!db.input.originalParent) {
      db.input.originalParent = db.input.parentNode;
    }

    // Remove craqbox on escape key.
    $(db.input).bind('keyup', Drupal.quicksearch.removeEsc);
    
    // Display in craqbox.
    Drupal.quicksearch.craqbox.build({
        content: $('<div>').append($(db.input)).append($('<div>').html(results)),
        completeCallback: function() {
          db.input.focus();
          Drupal.quicksearch.pagerSetup(db);          
          Drupal.quicksearch.tabSetup(db);
        },
        removeCallback: function() {
          Drupal.quicksearch.remove(db.input);
        }
    });
  }
};

/**
 *  Check if a form is a quicksearch form.
 */
Drupal.quicksearch.form = function(id) {
  for (var i in Drupal.settings.quicksearch.fields) {
    field = 'edit-'+ Drupal.settings.quicksearch.fields[i].replace(/_/g, '-') +'-keys';
    if (id == field) {
      return true;
    }
  }
};

/**
 *  Remove the newly searched content.
 */
Drupal.quicksearch.remove = function(input) {
  if (Drupal.settings.quicksearch.selector) {
    // Remove the search results and bring back the original page content.
    $(Drupal.settings.quicksearch.selector).html('').append($('#quicksearch_cache').children());
    $('#quicksearch_cache').remove();
  } else {
    // Save the original display because one faded in, jQuery will set the display = block even if it was inline.
    $(input)
    .attr('originalDisplay', $(input).css('display'))
    .css('display', 'none')
    .unbind('keyup', Drupal.quicksearch.removeEsc)
    .appendTo(input.originalParent)
    .fadeIn('fast', function() { 
      input.focus();
      $(input).css('display', $(input).attr('originalDisplay'));
    });
  }
}

/**
 *  Used by the textfield so the user can press escape to remove the UI version of the search results.
 */
Drupal.quicksearch.removeEsc = function(event) {
  if (event.keyCode == 27) {
    Drupal.quicksearch.craqbox.remove();
  }
}

/**
 *  These functions below are taken from autocomplete.js and slightly modified to work for live searching.
 *  One noted improvement is that if it is a search autocomplete, use drupal's search module setting for min characters allowed in a search.
 *  Live search requests are also cached like normal autocomplete calls.
 */

/**
 * Performs a cached and delayed search
 */
Drupal.ACDB.prototype.search = function (searchString, input) {
  var db = input ? input : this;
  db.searchString = searchString;
  var quicksearch = Drupal.quicksearch.form(db.owner.input.id);

  // Check the search module's min character count for a search so we don't get errors.
  if (quicksearch && searchString.length < Drupal.settings.quicksearch.minCharacters) {
    return false;
  }

  // See if db key has been searched for before
  if (db.cache[searchString]) {
    return db.owner.found(db.cache[searchString]);
  }

  // Initiate delayed search
  if (db.timer) {
    clearTimeout(db.timer);
  }
  db.timer = setTimeout(function() {
    db.owner.setStatus('begin');

    // Set a search type of node or user.
    if (quicksearch && Drupal.quicksearch.searchType) {
      db.uri = Drupal.settings.quicksearch.searchPath +'/'+ Drupal.quicksearch.searchType;
    }

    // Ajax GET request for autocompletion
    $.ajax({
      type: "GET",
      url: db.uri +'/'+ Drupal.encodeURIComponent(searchString),
      success: function (data) {
        // Custom solution for live searching.
        if (quicksearch && data) {
          db.cache[searchString] = data;
          // Verify if these are still the matches the user wants to see
          if (db.searchString == searchString) {
            db.owner.found(data);
          }
          db.owner.setStatus('found');
        } else {
          // Parse back result
          var matches = Drupal.parseJson(data);
          if (typeof matches['status'] == 'undefined' || matches['status'] != 0) {
            db.cache[searchString] = matches;
            // Verify if these are still the matches the user wants to see
            if (db.searchString == searchString) {
              db.owner.found(matches);
            }
            db.owner.setStatus('found');
          }
        }
      },
      error: function (xmlhttp) {
        alert('An HTTP error '+ xmlhttp.status +' occured.\n'+ db.uri);
      }
    });
  }, (quicksearch) ? Drupal.settings.quicksearch.uiDelay : db.delay);
}

/**
 * Fills the suggestion popup with any matches received
 */
Drupal.jsAC.prototype.found = function (matches) {
  // If no value in the textfield, do not show the popup.
  if (!this.input.value.length) {
    return false;
  }
  
  // Custom solution for live searching.
  if (Drupal.quicksearch.form(this.input.id)) {
    // We're not using the popup so just hide it.
    if (this.popup) {
      $(this.popup).css({visibility: 'hidden'});
    }
    // Do not redraw because we now have the search textfield in this element and this would break functionality.
    if (this.lastMatches != matches) {
      Drupal.quicksearch.display(this, matches);
      this.lastMatches = matches;
    }
  } else {
    // Prepare matches
    var ul = document.createElement('ul');
    var ac = this;
    for (key in matches) {
      var li = document.createElement('li');
      $(li)
      .html('<div>'+ matches[key] +'</div>')
      .mousedown(function () { ac.select(this); })
      .mouseover(function () { ac.highlight(this); })
      .mouseout(function () { ac.unhighlight(this); });
      li.autocompleteValue = key;
      $(ul).append(li);
    }

    // Show popup with matches, if any
    if (this.popup) {
      if (ul.childNodes.length > 0) {
        $(this.popup).empty().append(ul).show();
      }
      else {
        $(this.popup).css({visibility: 'hidden'});
        this.hidePopup();
      }
    }
  }
}

// Global Killswitch.
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.quicksearch.autoAttach);
}
