Quick Search
Developed by: Steve McKenzie (steve [at] stevemckenzie.ca)
Sponsored By: HerFabLife (herfablife.com)

Installation:
- Administer -> Site building -> Modules - to install
- Administer -> User management -> Access - to set up user permissions
- Administer -> Site configuration -> Quick search - to configure how Quick Search will work and display

Functionality:
- Adds on top of autocomplete.js for a much richer version of autocomplete for live searching.
